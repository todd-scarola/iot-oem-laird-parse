'use strict';
exports.processData = async(event) => {

   //parse Laird packet
   var message = await this.parseMessage(event.messageBody);

   //add battery percent
   message.batteryPercent = Number(await this.parseBattery(message.fwVersion, message.batteryLevel));

   //add range
   for (let i = 0; i < message.entries.length; i++) {
      var entry = message.entries[i];
      for (let j = 0; j < entry.logs.length; j++) {
         var log = entry.logs[j];
         if (log.recordType == 17) {
            log.log.range = Number(await this.parseRange(log.log.rssi));
         }
      }
   }

   //add event info
   message.gatewayId = event.gatewayId;
   message.messageType = 'tagData';
   message.receivedAt = event.receivedAt;

   return message;
};

exports.parseMessage = async(data) => {
   const Parser = require('binary-parser').Parser;

   const reverseBytes = (input) => {
      let result = '';
      for (let i = 0; i < input.length; i = i + 2) {
         result = input.substr(i, 2) + result;
      }
      return result;
   };

   let buf = Buffer.from(data, 'base64');
   const protocolVersion = buf.readUInt16LE(0);
   // console.log(protocolVersion)

   // Start POM Parser 2
   // parses each 8 byte log
   const logParser8 = new Parser()
      .endianess('little')
      .seek(2) // 2 bytes reserved
      .uint16('delta') // scan count since first record timestamp
      .int8('rssi')
      .uint8('motion')
      .int8('txPower');

   // parses each 4 byte log
   const logParser4 = new Parser()
      .endianess('little')
      .int8('rssi')
      .uint8('motion')
      .int8('txPower');

   const logParser = new Parser()
      .endianess('little')
      .uint8('recordType')
      .choice('log', {
         tag: 'recordType',
         choices: {
            16: logParser4,
            17: logParser8
         }
      });

   // array of entries
   const entryParser = new Parser()
      // log entry header
      .endianess('little')
      .uint8('entryStart') //A5
      .uint8('flags')
      .uint16('scanInterval')
      .string('serial', {
         encoding: 'hex',
         length: 6,
         formatter: reverseBytes
      })
      .uint32('timestamp') //30
      .uint16('length'); //32

   if (protocolVersion === 2) {
      entryParser.array('logs', {
         type: logParser,
         lengthInBytes: function() {
            return this.length - 16;
         }
      });
   }
   else if (protocolVersion === 1) {
      entryParser.array('logs', {
         type: logParser,
         readUntil: 'eof'
      });
   }

   // parses the main payload
   const pomParser = new Parser()
      .endianess('little')
      // header
      .uint16('entryProtocolVersion') //2
      .string('deviceId', { //8
         encoding: 'hex',
         length: 6,
         formatter: reverseBytes
      })
      .uint32('deviceTime') //12
      .uint32('lastUploadTime'); //16

   const headerVersionCheckByte = buf.readUInt8(16);
   // For latest header version, add some fields
   // 0xA5 at pos 16 means this is prior v2 protocol
   // without battery_level, network_id and fw_version
   // otherwise we do add parsers for those fields here

   if (headerVersionCheckByte != 0xA5) {
      pomParser
         .string('fwVersion', { //16
            encoding: 'hex',
            length: 4
         })
         .uint8('batteryLevel', { formatter: function(val) { return (val << 4); } }) //20 (left-shift by 4 to re-scale to millivolts)
         .uint16('networkId'); //22
   }

   pomParser
      .array('entries', {
         type: entryParser,
         readUntil: 'eof'
      });

   try {
      let jsonPayload = pomParser.parse(buf);
      return jsonPayload;
   }
   catch (error) {
      console.error(error);
   }

};

exports.parseBattery = async(fwVersion, batteryLevel) => {

   //need to switch based on fwversion
   let versions = ['00030000', '30000', '3c00b602', '00010400', '0.3.0', '00010700'];
   if (versions.includes(fwVersion)) {

      if (Number(batteryLevel) > 2900) { return 100; }
      else if (2500 < batteryLevel && batteryLevel < 2900) { return Math.floor(((parseInt(batteryLevel) - 2500) / 400) * 70) + 30 }
      else if (2400 <= batteryLevel && batteryLevel < 2500) { return 30; }
      else if (2250 <= batteryLevel && batteryLevel < 2400) { return 20; }
      else if (2000 <= batteryLevel && batteryLevel < 2250) { return 10; }
      else if (batteryLevel == 0) { return -1 }
      else if (batteryLevel <= 100) { return batteryLevel }
      else { return 10; }
   }
   else {

      if (batteryLevel > 3000) { return 100; }
      else if (2900 < batteryLevel && batteryLevel < 3000) { return 80; }
      else if (2850 <= batteryLevel && batteryLevel < 2900) { return 60; }
      else if (2800 <= batteryLevel && batteryLevel < 2850) { return 40; }
      else if (2700 <= batteryLevel && batteryLevel < 2800) { return 20; }
      else if (batteryLevel <= 100) { return batteryLevel; }
      else { return -1; }
   }
};

exports.parseRange = async(rssi) => {
   if (-20 <= rssi && rssi <= 0) { return 0 } //very close
   if (-70 <= rssi && rssi < -20) { return 1 } //close
   if (-99 <= rssi && rssi < -70) { return 2 } //mid-range
   if (rssi < -99) { return 3 } //far
   return 999; //unknown
};


/*
{
  "entryProtocolVersion": 2,
  "deviceId": "fa0ca0ec8ba7",
  "deviceTime": 1605799882,
  "lastUploadTime": 1605795629,
  "fwVersion": "00020800",
  "batteryLevel": 2944,
  "networkId": 65535,
  "entries": [
    {
      "entryStart": 165,
      "flags": 253,
      "scanInterval": 30,
      "serial": "e9ed73c7708a",
      "timestamp": 1605795682,
      "length": 256,
      "logs": [
        {
          "recordType": 17,
          "log": {
            "delta": 0,
            "rssi": -38,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 32,
            "rssi": -37,
            "motion": 0,
            "txPower": 0
          }
        }
      ]
    }
  ],
  "gatewayId": "354616090321758",
  "messageType": "tagData"
}
*/
