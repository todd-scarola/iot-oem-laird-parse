exports.processData = async(event) => {

   var message = {
      messageType: 'gatewayStatus',
      gatewayId: event.gatewayId.slice(9),
      eventTime: new Date(event.messageBody.timestamp * 1000),
      firmware: event.messageBody.version,
      rssi: event.messageBody.state.reported.radio_rssi || 0,
      sinr: event.messageBody.state.reported.radio_sinr || 0,
   };

   return message;
};
