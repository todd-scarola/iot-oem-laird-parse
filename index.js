const AWS = require('aws-sdk');
const SNS = new AWS.SNS();

exports.handler = async(event) => {
    var responses = [];

    if (event.hasOwnProperty('Records'))
    {
        for (const record of event.Records) {
            //SNS event
            if (record.EventSource == 'aws:sns') {
                var body = JSON.parse(record.Sns.Message);
                if (body) { responses.push(await this.processMessage(body)); }
            }
        }
        return responses;
    }
    else
    //normal JSON request
    {
        return await this.processMessage(event);
    }
};

exports.processMessage = async(event) => {

   //get processing function
   var helper = null;
   if (event.hasOwnProperty('messageType')) {
      try {
         helper = require('./messages/' + event.messageType + '/main.js');
      }
      catch (error) {
         console.error(error);
      }
   }

   if (helper) {
      var message = await helper.processData(event);
      if (message) {
         //write to SQS
         if (await this.sendSns(JSON.stringify(message))) {
            console.log('sent SQS: %j', message);
            return message;
         }
      }
   }
};

//send message to SNS
exports.sendSns = async function(message) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            "Message": message,
            "TopicArn": process.env.SNS_ARN
         };

         SNS.publish(params, function(err, data) {
            if (err) {
               console.log('SNS error: %s', err);
               reject(err);
            }
            else {
               resolve(data);
            }
         });
      }
   );
};